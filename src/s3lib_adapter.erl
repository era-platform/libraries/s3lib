%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>, Pavel Abramov
%%% @date 16.06.2021
%%% @doc RP-736 S3 Storage Utility. Use erlcloud_s3.

-module(s3lib_adapter).
-author(['Peter Bukashin <tbotc@yandex.ru>','Abramov Pavel']).

-export([
    get_size/3,
    get_binary/3,
    put_binary/4, put_binary/5,
    get_file/4,
    put_file/4, put_file/5,
    delete/3
]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include_lib("kernel/include/file.hrl").

-include_lib("erlcloud/include/erlcloud_aws.hrl").

-define(MinSizeAllowed, 5242880).
-define(RequestRetryCount, 5).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---------------------------
%% Head request to s3. Read size feild.
%% ---------------------------
-spec get_size(Bucket::list(), Key::list(), Opts::map()) -> {ok, Size::non_neg_integer()} | {error, binary()}.
%% ---------------------------
get_size(Bucket, Key, Opts) when is_list(Bucket), is_list(Key) ->
    {Config,S3Opts} = prepare_config_and_opts(Opts),
    get_data_size(Bucket, Key, S3Opts, Config, ?RequestRetryCount);
get_size(Bucket, Key, Opts) ->
    get_size(?BU:to_list(Bucket), ?BU:to_list(Key), Opts).

%% ---------------------------
%% Return tuple list. example: [{last_modified,"Wed, 04 Jul 2018 10:19:37 GMT"},{content_length,"5"},{content,<<"test2">>}, ...]
%% Data is found by the key 'content'.
%% ---------------------------
-spec get_binary(Bucket::list(), Key::list(), Opts::map()) -> {ok, Result::list()} | {error, binary()}.
%% ---------------------------
get_binary(Bucket, Key, Opts) when is_binary(Bucket), is_binary(Key), is_map(Opts) ->
    get_binary(?BU:to_list(Bucket), ?BU:to_list(Key), Opts);
get_binary(Bucket, Key, Opts) when is_list(Bucket), is_list(Key), is_map(Opts) ->
    {Config,S3Opts} = prepare_config_and_opts(Opts),
    case get_data_size(Bucket, Key, S3Opts, Config, ?RequestRetryCount) of
        {error, _}=Error1 -> Error1;
        {ok, Size} ->
            case (Size > ?MinSizeAllowed) of
                true -> {error, <<"Very large data size (", (?BU:to_binary(Size))/binary,"). Max ", (?BU:to_binary(?MinSizeAllowed))/binary, $.>>};
                false -> get_binary_do(Bucket, Key, S3Opts, Config, ?RequestRetryCount)
            end
    end.

%% ---------------------------
-spec put_binary(Bucket::list() | binary(), Key::list() | binary(), Data::binary(), Opts::map()) -> {ok, Result::list()} | {error, binary()}.
%% ---------------------------
put_binary(Bucket, Key, Data, Opts) ->
    put_binary(Bucket, Key, Data, Opts, []).

%% ---------------------------
%% Put binary data.
%% Return tuple list. example: [{"server","AmazonS3"},{"date","Wed, 04 Jul 2018 10:26:16 GMT"}, ...]
%% ---------------------------
-spec put_binary(Bucket::list() | binary(), Key::list() | binary(), Data::binary(), Opts::map(), Headers::list()) -> {ok, Result::list()} | {error, binary()}.
%% ---------------------------
put_binary(Bucket, Key, Data, Opts, Headers) when is_binary(Bucket), is_binary(Key), is_binary(Data), is_map(Opts) ->
    put_binary(?BU:to_list(Bucket), ?BU:to_list(Key), Data, Opts, Headers);
put_binary(Bucket, Key, Data, Opts, Headers) when is_list(Bucket), is_list(Key), is_binary(Data), is_map(Opts) ->
    case (erlang:byte_size(Data) > ?MinSizeAllowed) of
        true -> {error, <<"Very large data size. Max ", (?BU:to_binary(?MinSizeAllowed))/binary, $.>>};
        false -> try_put_binary(Opts, Bucket, Key, Data, Headers)
    end.

%% ---------------------------
%% Get data and write to file.
%% ---------------------------
-spec get_file(Bucket::list(), Key::list(), Filepath::list(), Opts::map()) -> {ok, Bytes::non_neg_integer()} | {error, binary()}.
%% ---------------------------
get_file(Bucket, Key, Filepath, Opts) when is_list(Bucket), is_list(Key), is_list(Filepath), is_map(Opts) ->
    {Config,S3Opts} = prepare_config_and_opts(Opts),
    ChunkSize = maps:get(chunk_size, Opts, ?MinSizeAllowed),
    UpChunkSize = case (ChunkSize < ?MinSizeAllowed) of true -> ?MinSizeAllowed; false -> ChunkSize end,
    ResultStep1 = get_file_step_1(get_data_size(Bucket, Key, S3Opts, Config, ?RequestRetryCount), Key),
    ResultStep2 = get_file_step_2(ResultStep1, Bucket, Key, S3Opts, Config, UpChunkSize),
    ResultStep3 = get_file_step_3(ResultStep2, Bucket, Key, Config),
    ResultStep4 = get_file_step_4(ResultStep3, Filepath),
    ResultStep4.

%% ---------------------------
%% Put file multipart. Set option chunk_size.
%% ---------------------------
-spec put_file(Bucket::list(), Key::list(), Filepath::list(), Opts::map()) -> {ok, Result::list()} | {error, binary()}.
%% ---------------------------
put_file(Bucket, Key, Filepath, Opts) ->
    put_file(Bucket, Key, Filepath, Opts, []).

%% ---------------------------
%% Put file multipart. Set option chunk_size.
%% ---------------------------
-spec put_file(Bucket::list(), Key::list(), Filepath::list(), Opts::map(), Headers::list()) -> {ok, Result::list()} | {error, binary()}.
%% ---------------------------
put_file(Bucket, Key, Filepath, Opts, Headers) when is_list(Bucket), is_list(Key), is_list(Filepath), is_map(Opts), is_list(Headers) ->
    ChunkSize = maps:get(chunk_size, Opts, ?MinSizeAllowed),
    case pre_put_file(Filepath, ChunkSize) of
        {error, _}=Error -> Error;
        {small, Bin} -> put_binary(Bucket, Key, Bin, Opts, Headers);
        {big, Temp} ->
            case file:open(Temp, [read, binary]) of
                {error, Error1} ->
                    ?LOG('$error',"~p -- put_file: open file (~120p) Error: ~120p", [?MODULE, Temp, Error1]),
                    {error, <<"Can not read file">>};
                {ok, Io} ->
                    Result = try_put_file(Bucket, Key, Io, ChunkSize, Opts, Headers),
                    file:close(Io),
                    file:delete(Temp),
                    Result
            end
    end.

%% ---------------------------
%% Return tuple list. example: [{delete_marker,false},{version_id,"null"}]
%% ---------------------------
-spec delete(Bucket::list(), Key::list(), Opts::map()) -> {ok, Result::list()} | {error, binary()}.
%% ---------------------------
delete(Bucket, Key, Opts) when is_binary(Bucket), is_binary(Key), is_map(Opts) ->
    delete(?BU:to_list(Bucket), ?BU:to_list(Key), Opts);
delete(Bucket, Key, Opts) when is_list(Bucket), is_list(Key), is_map(Opts) ->
    {Config,_} = prepare_config_and_opts(Opts),
    try
        {ok, erlcloud_s3:delete_object(Bucket, Key, Config)}
    catch
        Exception:Error:ST ->
            ?LOG('$crash',"~p -- delete: Exception: ~120p~n\t~120p~n\tStack: ~140tp", [?MODULE, Exception, Error, ST]),
            {error, <<"erlcloud_s3 delete exception">>}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---------------------------
%%
%% ---------------------------
try_put_binary(Opts, Bucket, Key, Data, HeadersRaw) ->
    {Config,S3Opts} = prepare_config_and_opts(Opts),
    Headers = prepare_headers(HeadersRaw),
    try
        case erlcloud_s3:put_object(?BU:to_list(Bucket), Key, Data, S3Opts, Headers, Config) of
            {error, Error1} ->
                ?LOG('$error',"~p -- put: put_object error: ~120p", [?MODULE, Error1]),
                {error, <<"erlcloud_s3 put_object error">>};
            Result -> {ok, Result}
        end
    catch
        Exception:Error2:StackTrace ->
            ?LOG('$crash',"~p -- put: Exception: ~120p~n\t~120p~n\t~120p", [?MODULE, Exception, Error2, StackTrace]),
            {error, ?BU:strbin("erlcloud_s3 put ~tp:~tp", [Exception,Error2])}
    end.

%% ---------------------------
%%
%% ---------------------------
prepare_config_and_opts(Params) ->
    Config = prepare_s3_config(Params),
    Opts = prepare_s3_opts(Params),
    {Config,Opts}.

%% @private
prepare_s3_config(Params) ->
    Keys = record_info(fields, aws_config),
    F = fun(K, {I, R, M}) -> case maps:get(K, Params, undefined) of undefined -> {I + 1, R, M}; V -> {I + 1, setelement(I, R, convert_type(K,V)), M#{K=>convert_type(K,V)}} end end,
    {_, Config, _} = lists:foldl(F, {2, #aws_config{s3_follow_redirect=true, aws_region='eu-central-1'}, #{}}, Keys),
    Config.

%% @private
prepare_s3_opts(Params) ->
    CfgKeys = record_info(fields, aws_config),
    Opts = maps:without(CfgKeys,Params),
    F = fun(K,V,Acc) when K=='acl' -> Acc#{K => convert_type(acl,V)};
           (K,V,Acc) -> Acc#{K=>V}
        end,
    Opts1 = maps:fold(F,#{},Opts),
    ?BU:map_to_json(Opts1).

%% @private
convert_type(s3_bucket_access_method, V) ->
    case catch ?BU:to_binary(V) of
        <<"vhost">> -> vhost;
        <<"path">> -> path;
        <<"auto">> -> auto;
        _ -> auto
    end;
convert_type(acl, V) ->
    case ?BU:to_binary(V) of
        <<"undefined">> -> 'undefined';
        <<"private">>   -> 'private';
        <<"public_read">> -> 'public_read';
        <<"public_read_write">> -> 'public_read_write';
        <<"authenticated_read">> -> 'authenticated_read';
        <<"bucket_owner_read">> -> 'bucket_owner_read';
        <<"bucket_owner_full_control">> -> 'bucket_owner_full_control';
        _ -> 'undefined'
    end;
convert_type(_, V) -> V.

%% ---
prepare_headers(Headers) ->
    [{?BU:to_lower(K),V} || {K,V} <- Headers].

%% ---------------------------
%% Check file size. If small, then return binary. If big, then copy to temp dir.
%% ---------------------------
-spec pre_put_file(list(), non_neg_integer()) -> {big, list()} | {small, binary()} | {error, binary()}.
%% ---------------------------
pre_put_file(Filepath, ChunkSize) ->
    case file:read_file_info(Filepath) of
        {error, Error1} ->
            ?LOG('$error', "~p -- pre_put_file: read_file_info Error: ~120p", [?MODULE, Error1]),
            {error, <<"Fail read file info">>};
        {ok, #file_info{size=Size}} ->
            case (Size > ?MinSizeAllowed) of
                true -> case copy_to_temp(Filepath) of
                            error -> {error, <<"Fail copy to temp file">>};
                            {ok, Temp} ->
                                ?LOG('$trace', "~p -- put_file: size: ~p, parts: ~p", [?MODULE, Size, (Size div ChunkSize)+1]),
                                {big, Temp}
                        end;
                false -> case file:read_file(Filepath) of
                             {error, Error2} ->
                                 ?LOG('$error', "~p -- pre_put_file: read_file Error: ~120p", [?MODULE, Error2]),
                                 {error, <<"Fail read file">>};
                             {ok, Binary} -> {small, Binary}
                         end
            end
    end.
%% @private
copy_to_temp(Filepath) ->
    FileName = filename:basename(Filepath),
    Temp = filename:join("temp", FileName),
    LogFun = fun({error, Reason}) -> ?LOG('$error', "~p -- copy_to_temp: error: ~120p (~120p -> ~120p)", [?MODULE, Reason, Filepath, Temp]), error end,
    case filelib:ensure_dir(Temp) of
        ok -> case file:copy(Filepath, Temp) of {ok, _} -> {ok, Temp}; Error1 -> LogFun(Error1) end;
        Error2 -> LogFun(Error2)
    end.

%% ---------------------------
%% Read and send file on pieces.
%% ---------------------------
try_put_file(Bucket, Key, Io, ChunkSize, Opts, Headers)->
    try
        put_file_do(Bucket, Key, Io, ChunkSize, Opts, Headers)
    catch
        Exception:Error2:StackTrace ->
            ?LOG('$crash', "~p -- put_file: ~120p:~120p~n\tStack: ~120p", [?MODULE, Exception, Error2, StackTrace]),
            {error, <<"Catch exception. See log.">>}
    end.
%% @private
put_file_do(Bucket, Key, IoDevice, ChunkSize, Opts, HeadersRaw) ->
    {Config,S3Opts} = prepare_config_and_opts(Opts),
    Headers = prepare_headers(HeadersRaw),
    case erlcloud_s3:start_multipart(Bucket, Key, S3Opts, Headers, Config) of
        {error, Reason} ->
            ?LOG('$error', "~p -- put_file_do: start_multipart error: ~120p", [?MODULE, Reason]),
            {error, <<"Start multipart error">>};
        {ok, Props} ->
            {_, Uid} = lists:keyfind(uploadId, 1, Props),
            put_file_do_acc(Bucket, Key, IoDevice, ChunkSize, S3Opts, Headers, Config, Uid, 1, [])
    end.
%% @private
put_file_do_acc(Bucket, Key, IoDevice, ChunkSize, S3Opts, Headers, Config, Uid, PartNumber, Acc) ->
    case file:read(IoDevice, ChunkSize) of
        eof ->
            case erlcloud_s3:complete_multipart(Bucket, Key, Uid, Acc, Headers, Config) of
                {error, Reason3} ->
                    ?LOG('$errr', "~p -- put_file_do_acc: complete error: ~120p", [?MODULE, Reason3]),
                    {error, <<"Upload complete error">>};
                ok -> {ok, []}
            end;
        {error, Reason} ->
            erlcloud_s3:abort_multipart(Bucket, Key, Uid, S3Opts, Headers, Config),
            ?LOG('$error', "~p -- put_file_do_acc: read file error: ~120p", [?MODULE, Reason]),
            {error, <<"Read file error">>};
        {ok, Data} ->
            case retry_upload_part(Bucket, Key, Uid, PartNumber, Data, Headers, Config, ?RequestRetryCount) of
                {ok,NewPartNumber,Etag} ->
                    Acc1 = Acc ++ [{PartNumber, Etag}],
                    put_file_do_acc(Bucket, Key, IoDevice, ChunkSize, S3Opts, Headers, Config, Uid, NewPartNumber, Acc1);
                {error,_}=Err -> Err
            end
    end.

%% @private
retry_upload_part(_Bucket, _Key, _Uid, _PartNumber, _Data, _Headers, _Config, 0) -> {error, <<"Upload part error">>};
retry_upload_part(Bucket, Key, Uid, PartNumber, Data, Headers, Config, RetryCount) ->
    case erlcloud_s3:upload_part(Bucket, Key, Uid, PartNumber, Data, Headers, Config) of
        {error, Reason2} ->
            ?LOG('$error', "~p -- put_file_do_acc: upload_part ~120p error: ~120p", [?MODULE, PartNumber, Reason2]),
            retry_upload_part( Bucket, Key, Uid, PartNumber, Data, Headers, Config, RetryCount - 1);
        {ok, Props} ->
            {_, Etag} = lists:keyfind(etag, 1, Props),
            NewPartNumber = PartNumber + 1,
            {ok,NewPartNumber,Etag}
    end.

%% ---------------------------
%% Head request to s3.
%% ---------------------------
get_data_size(_Bucket, _Key, _S3Opts, _Config, 0) -> {error, <<"Failed get data size.">>};
get_data_size(Bucket, Key, S3Opts, Config, Count) ->
    try
        Props = erlcloud_s3:head_object(Bucket, Key, S3Opts, Config),
        {_, Size} = lists:keyfind(content_length, 1, Props),
        {ok, ?BU:to_int(Size)}
    catch
        Exception:Error:StackTrace ->
            ?LOG('$crash', "~p -- pre_get_binary: head object Exception: ~120p:~120p~n\tStacktrace: ~120tp", [?MODULE, Exception, Error, StackTrace]),
            get_data_size(Bucket, Key, S3Opts, Config, Count-1)
    end.

%% ---------------------------
%% Retry get object requests.
%% ---------------------------
get_binary_do(_Bucket, _Key, _S3Opts, _Config, 0) -> {error, <<"Failed get data from S3.">>};
get_binary_do(Bucket, Key, S3Opts, Config, Count) ->
    try
        case erlcloud_s3:get_object(Bucket, Key, S3Opts, Config) of
            {error, Error1} ->
                ?LOG('$error', "~p -- get_binary_do: response error: ~120p", [?MODULE, Error1]),
                get_binary_do(Bucket, Key, S3Opts, Config, Count-1);
            Result -> {ok, Result}
        end
    catch
        Exception:Error2:ST ->
            ?LOG('$crash', "~p -- get_binary_do: Exception: ~120p:~120p~n\tStack: ~140tp", [?MODULE, Exception, Error2, ST]),
            get_binary_do(Bucket, Key, S3Opts, Config, Count-1)
    end.

%% ---------------------------
%% Prepare temp file
%% ---------------------------
get_file_step_1({error,_}=Error, _Key) -> Error;
get_file_step_1({ok, DataSize}, Key) ->
    FileName = filename:basename(Key),
    Temp = filename:join("temp", FileName),
    case filelib:ensure_dir(Temp) of
        ok -> {ok, Temp, DataSize};
        {error, Reason} ->
            ?LOG('$error',"~p -- get_file_step_1: error: ~120p (~120p)", [?MODULE, Reason, Temp]),
            {error, <<"Failed create temp dir.">>}
    end.
%% ---------------------------
%% Download file by pieces to temp.
%% ---------------------------
get_file_step_2({error,_}=Error, _Bucket, _Key, _S3Opts, _Config, _ChunkSize) -> Error;
get_file_step_2({ok, Temp, DataSize}, Bucket, Key, S3Opts, Config, ChunkSize) ->
    case (DataSize > ?MinSizeAllowed) of
        true -> case get_file_by_parts(Temp, Bucket, Key, S3Opts, Config, ChunkSize) of {error, _}=Error1 -> Error1; ok -> {ok, Temp} end;
        false -> case get_file_wholly(Temp, Bucket, Key, S3Opts, Config) of {error, _}=Error2 -> Error2; ok -> {ok, Temp} end
    end.

%% @private
get_file_wholly(Temp, Bucket, Key, S3Opts, Config) ->
    case get_binary_do(Bucket, Key, S3Opts, Config, ?RequestRetryCount) of
        {error, _}=Error1 -> Error1;
        {ok, Props} ->
            {_, Data} = lists:keyfind(content, 1, Props),
            case file:write_file(Temp, Data) of
                {error, Reason} ->
                    ?LOG('$error',"~p -- get_file_wholly: error write temp file ~120p: ~120p", [?MODULE, Temp, Reason]),
                    {error, <<"Failed write temp file.">>};
                ok -> ok
            end
    end.

%% @private
get_file_by_parts(Temp, Bucket, Key, S3Opts, Config, ChunkSize) ->
    case file:open(Temp, [write, binary]) of
        {error, Reason} ->
            ?LOG('$error',"~p -- get_file_by_parts: error open temp file ~120p: ~120p", [?MODULE, Temp, Reason]),
            {error, <<"Failed open temp file.">>};
        {ok, IO} ->
            Result = get_file_by_parts_do(IO, Bucket, Key, S3Opts, Config, ChunkSize, 0),
            file:close(IO),
            Result
    end.

%% @private
get_file_by_parts_do(IO, Bucket, Key, S3Opts, Config, ChunkSize, LoadSize) ->
    Range = <<"bytes=", (?BU:to_binary(LoadSize))/binary, $-, (?BU:to_binary(LoadSize+ChunkSize))/binary>>,
    case retry_get_part(Bucket, Key, S3Opts, Config, Range, ?RequestRetryCount) of
        {error, _}=Error1 -> Error1;
        {ok, Data, Length} ->
            case file:write(IO, Data) of
                {error, Reason} ->
                    ?LOG('$error',"~p -- get_file_by_parts_do: write temp file error: ~120p", [?MODULE, Reason]),
                    {error, <<"Failed write to temp file.">>};
                ok ->
                    case (Length < ChunkSize) of
                        true -> ok;
                        false -> get_file_by_parts_do(IO, Bucket, Key, S3Opts, Config, ChunkSize, LoadSize+Length)
                    end
            end
    end.
%% @private
retry_get_part(_Bucket, _Key, _S3Opts, _Config, _Range, 0) -> {error, <<"Could not get part of the file.">>};
retry_get_part(Bucket, Key, S3Opts, Config, Range, Count) ->
    try
        RangeL = ?BU:to_list(Range),
        case erlcloud_s3:get_object(Bucket, Key, [{range, RangeL}|S3Opts], Config) of
            {error, Error1} ->
                ?LOG('$error',"~p -- retry_get_part: get_object response error: ~120p", [?MODULE, Error1]),
                retry_get_part(Bucket, Key, S3Opts, Config, Range, Count-1);
            Props ->
                {_, Length} = lists:keyfind(content_length, 1, Props),
                {_, Data} = lists:keyfind(content, 1, Props),
                {ok, Data, ?BU:to_int(Length)}
        end
    catch
        Exception:Error2:ST ->
            ?LOG('$crash',"~p -- retry_get_part: get_object exception: ~120p:~120p~n\tStack: ~140tp", [?MODULE, Exception, Error2, ST]),
            retry_get_part(Bucket, Key, S3Opts, Config, Range, Count-1)
    end.

%% ---------------------------
%% Check md5.
%% ---------------------------
get_file_step_3({error,_}=Error, _Bucket, _Key, _Config) -> Error;
get_file_step_3({ok, Temp}, _Bucket, _Key, _Config) -> {ok, Temp}.

%% ---------------------------
%% Copy temp to real path.
%% ---------------------------
get_file_step_4({error,_}=Error, _Filepath) -> Error;
get_file_step_4({ok, Temp}, Filepath) ->
    case filelib:ensure_dir(Filepath) of
        ok ->
            case file:copy(Temp, Filepath) of
                {error, Reason1} ->
                    ?LOG('$error',"~p -- get_file_step_4: file copy error: ~120p (~120p)", [?MODULE, Reason1, Filepath]),
                    {error, <<"Failed copy from temp file.">>};
                Result ->
                    file:delete(Temp),
                    Result
            end;
        {error, Reason2} ->
            ?LOG('$error',"~p -- get_file_step_4: error: ~120p (~120p)", [?MODULE, Reason2, Filepath]),
            {error, <<"Failed create dir.">>}
    end.
