%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.05.2021
%%% @doc reglib application (registrar of global names).
%%%      Store data in local ets only.
%%%      Sync data to other replicas.
%%%      Name::term() -> Pid::pid(), Name::term() -> [Pid::pid()]
%%%   Application could be setup by application:set_env:
%%%      log_destination
%%%          descriptor to define log file folder and prefix.
%%%          Default: {'env','s3'}

-module(s3lib).
-author('Peter <tbotc@yandex.ru>').

-behaviour(application).

%% facade
-export([
    get_size/3,
    get_binary/3,
    put_binary/4, put_binary/5,
    put_file/4, put_file/5,
    get_file/4,
    delete/3
]).

%% configuration
-export([start/0,
         stop/0]).

%% application
-export([start/2,
         stop/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public
%% ====================================================================

%% -----------------------------------------------------
%% LOADER FACADE
%% -----------------------------------------------------

%% -------------------------------------
%% Starts application
%% -------------------------------------
start() ->
    case application:ensure_all_started(?APP, permanent) of
        {ok, _Started} -> ok;
        Error -> Error
    end.

%% -------------------------------------
%% Stops application
%% -------------------------------------
stop() -> application:stop(?APP).

%% -----------------------------------------------------
%% CLIENT FACADE
%% -----------------------------------------------------

%% ---------------------------
%% Head request to s3 storage. Read size feild.
%% ---------------------------
-spec get_size(Bucket::list() | binary(), Key::list() | binary(), Opts::map()) ->
    {ok, Size::non_neg_integer()} | {error, binary()}.
%% ---------------------------
get_size(Bucket, Key, Opts)
  when is_map(Opts)
    andalso (is_binary(Bucket) orelse is_list(Bucket))
    andalso (is_binary(Key) orelse is_list(Key)) ->
    ?S3:get_size(?BU:to_list(Bucket), ?BU:to_list(Key), Opts).

%% ---------------------------
%% Get binary data from s3 storage.
%% Return tuple list, ex: [{last_modified,"Wed, 04 Jul 2018 10:19:37 GMT"},{content_length,"5"},{content,<<"test2">>}, ...]
%% Data is found by the key 'content'.
%% ---------------------------
-spec get_binary(Bucket::list() | binary(), Key::list() | binary(), Opts::map()) ->
    {ok, Result::list()} | {error, binary()}.
%% ---------------------------
get_binary(Bucket, Key, Opts)
  when is_map(Opts)
    andalso (is_binary(Bucket) orelse is_list(Bucket))
    andalso (is_binary(Key) orelse is_list(Key)) ->
    ?S3:get_binary(?BU:to_list(Bucket), ?BU:to_list(Key), Opts).

%% ---------------------------
%% Put binary data to s3 storage.
%% Return tuple list, ex: [{"server","AmazonS3"},{"date","Wed, 04 Jul 2018 10:26:16 GMT"}, ...]
%% ---------------------------
-spec put_binary(Bucket::list() | binary(), Key::list() | binary(), Data::binary(), Opts::map()) ->
    {ok, Result::list()} | {error, binary()}.
%% ---------------------------
put_binary(Bucket, Key, Data, Opts) ->
    put_binary(Bucket, Key, Data, Opts,[]).

%% ---------------------------
%% Put binary data to s3 storage.
%% Return tuple list, ex: [{"server","AmazonS3"},{"date","Wed, 04 Jul 2018 10:26:16 GMT"}, ...]
%% Allows setup specific headers
%% ---------------------------
-spec put_binary(Bucket::list() | binary(), Key::list() | binary(), Data::binary(), Opts::map(), Headers::list()) ->
    {ok, Result::list()} | {error, binary()}.
%% ---------------------------
put_binary(Bucket, Key, Data, Opts, Headers)
  when is_map(Opts) andalso is_binary(Data) andalso is_list(Headers)
  andalso (is_binary(Bucket) orelse is_list(Bucket))
  andalso (is_binary(Key) andalso is_list(Key)) ->
    ?S3:put_binary(?BU:to_list(Bucket), ?BU:to_list(Key), Data, Opts, Headers).

%% ---------------------------
%% Get data and write to file.
%% ---------------------------
-spec get_file(Bucket::list() | binary(), Key::list() | binary(), Filepath::list() | binary(), Opts::map()) ->
    {ok, Bytes::non_neg_integer()} | {error, binary()}.
%% ---------------------------
get_file(Bucket, Key, Filepath, Opts)
  when is_map(Opts)
  andalso (is_binary(Bucket) orelse is_list(Bucket))
  andalso (is_binary(Key) orelse is_list(Bucket))
  andalso (is_binary(Filepath) orelse is_list(Filepath)) ->
    ?S3:get_file(?BU:to_list(Bucket), ?BU:to_list(Key), ?BU:to_list(Filepath), Opts).

%% ---------------------------
%% Put file from local filepath to s3 storage. Set option chunk_size.
%% ---------------------------
-spec put_file(Bucket::list() | binary(), Key::list() | binary(), Filepath::list() | binary(), Opts::map()) ->
    {ok, Result::list()} | {error, binary()}.
%% ---------------------------
put_file(Bucket, Key, Filepath, Opts) ->
    put_file(Bucket, Key, Filepath, Opts, []).

%% ---------------------------
%% Put file from local filepath to s3 storage. Set option chunk_size.
%% Allows setup specific headers
%% ---------------------------
-spec put_file(Bucket::list() | binary(), Key::list() | binary(), Filepath::list() | binary(), Opts::map(), Headers::list()) ->
    {ok, Result::list()} | {error, binary()}.
%% ---------------------------
put_file(Bucket, Key, Filepath, Opts, Headers)
  when is_map(Opts) andalso is_list(Headers)
    andalso (is_binary(Bucket) orelse is_list(Bucket))
    andalso (is_binary(Key) orelse is_list(Key))
    andalso (is_binary(Filepath) orelse is_list(Filepath)) ->
    ?S3:put_file(?BU:to_list(Bucket), ?BU:to_list(Key), Filepath, Opts, Headers).

%% ---------------------------
%% Remove resource from s3 storage.
%% Return tuple list, ex. [{delete_marker,false},{version_id,"null"}]
%% ---------------------------
-spec delete(Bucket::list() | binary(), Key::list() | binary(), Opts::map()) ->
    {ok, Result::list()} | {error, binary()}.
%% ---------------------------
delete(Bucket, Key, Opts)
  when is_map(Opts)
    andalso (is_binary(Bucket) orelse is_list(Bucket))
    andalso (is_binary(Key) orelse is_list(Key)) ->
    ?S3:delete(?BU:to_list(Bucket), ?BU:to_list(Key), Opts).

%% ===================================================================
%% Application callback functions
%% ===================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_, _) ->
    setup_start_ts(),
    setup_dependencies(),
    ensure_deps_started(),
    ?OUT('$info',"~ts. ~p start", [?RoleName, self()]),
    ?SUPV:start_link(undefined).

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(State) ->
    ?OUT('$info',"~ts: Application stopped (~120p)", [?RoleName, State]),
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% --------------------------------------
%% @private
%% setup start_utc to applicaiton's env for monitor purposes
%% --------------------------------------
setup_start_ts() ->
    application:set_env(?APP,start_utc,?BU:timestamp()).

%% --------------------------------------
%% @private
%% setup opts of dependency apps
%% --------------------------------------
setup_dependencies() -> ok.

%% --------------------------------------
%% @private
%% starts deps applications, that are not linked to app, and to ensure
%% --------------------------------------
ensure_deps_started() ->
    {ok,_} = application:ensure_all_started(?BASICLIB, permanent),
    R = ssl:start(),
    ?OUT('$info', "ssl. start: ~140p", [R]),
    erlcloud:start(),
    lhttpc:start(),
    {ok,_} = application:ensure_all_started(erlcloud, permanent),
    {ok,_} = application:ensure_all_started(lhttpc, permanent).